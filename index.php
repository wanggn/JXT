<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>htmledit</title>

    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">

    <!-- 可选的Bootstrap主题文件（一般不用引入） -->
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">

    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>

    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="http://cdn.bootcss.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="/share/js/my/jxtcomm.js"></script>


    <script>
        var jxt = {};
    </script>
    <script src="/share/js/my/jxtMsg.js"></script>
    <script src="/share/js/my/jxtRoute.js"></script>

    <?php
    $page = array_key_exists("p", $_GET) ? $_GET['p'] : "1";
    ?>
    <script>
        var pageid = '<?php  echo $page; ?>';
    </script>
  
</head>

<body>
<br>
<div id="textview" class="container-fluid">
    <div class="row" id="_jxtroot_">
        <div class="col-md-12">

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="btn-toolbar" role="toolbar">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" onclick="localStorage.clear();">clear</button>

                </div>

                <div class="btn-group">
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','creatediv');">图文</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','createtextdiv');">文本</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','createlist',{tpname:'ulist'});">ulist</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','createlist',{tpname:'olist'});">olist</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','createtextlist',{tpname:'otextlist'});">otextlist</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','createtextlist',{tpname:'utextlist'});">utextlist</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','createtable');">表格</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','createform');">表单</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','createimglist');">图片组</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','createselfinfo');">个人信息</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','createjob_intention');">求职期望</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','createself_evaluation');">自我评价</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','creatework_experience');">工作经历</button>
                    <button type="button" class="btn btn-default" onclick="jxt.msg.radio('bar','createproject_experience');">项目经验</button>
                </div>
                <div class="btn-group" id="_mybar_">

                </div>
            </div>

        </div>

    </div>

</div>
<br/>
<iframe id="iframe" marginheight=0 marginwidth=0 scrolling=no></iframe>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/upload.php');
?>
<script>
    iframewoindow = document.getElementById("iframe").contentWindow;

    function changeFrameHeight() {
        var ifm = document.getElementById("iframe");
        ifm.height = document.documentElement.clientHeight;
        ifm.width = document.documentElement.clientWidth;
    }


    $(function ($) {
   

        $("#iframe").load(function () {
            //var mainheight = $(this).contents().find("body").height() + 30;
            //$(this).height(mainheight);
            changeFrameHeight();
        });

         window.onresize = function () {

            changeFrameHeight();
        };
        $("#iframe").attr("src", "page.php?p=" + pageid);


    });


</script>

</body>

</html>