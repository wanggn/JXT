 jxt.root.dataJson = { id: "_jxtroot_", type: "root", pageid: 0, title: "", css: "", init: { title: "标题" }, children: [] };
 jxt.pages.dataJson = { id: "_jxtpages_", type: "pages", css: "", title: "", init: { title: "标题" }, children: [] };

 jxt.text.dataJson = { id: "", type: 'text', css: "", init: { text: '文字' }, cssname: "", tpname: "" };
 jxt.div.dataJson = { id: "", type: 'div', css: "", cssname: "", tpname: "", init: {}, children: [] };
 jxt.textdiv.dataJson = {id: "", type: 'textdiv', css: "", cssname: "", tpname: "", init: {}, content: '文字'};
 jxt.img.dataJson = { id: "", type: 'img', src: "", url: "", css: "", init: { src: "", url: "" }, cssname: "", tpname: "" };
 jxt.file.dataJson = { id: "", type: 'file', text: "", url: "", css: "",  cssname: "", tpname: "" };
 jxt.link.dataJson = { id: "", type: 'link', text: "", url: "", css: "",  cssname: "", tpname: "" };
 jxt.list.dataJson = { id: "", type: 'list', css: "", init: { rowcount: 3 }, children: [], cssname: "", tpname: "" };
 jxt.textlist.dataJson = { id: "", type: 'textlist', init: { rowcount: 3 }, content: [], cssname: "", tpname: "" };
 jxt.table.dataJson = { id: "", type: 'table', css: "", init: { rowcount: 3, columncount: 3 }, children: [], cssname: "", tpname: "" };

 jxt.form.dataJson = { id: "", type: 'form', css: "", cssname: "", tpname: "",content:[{name: "姓名", value: ""}, {name: "年龄", value: ""}]};
 jxt.selfinfo.dataJson = { id: "", type: 'selfinfo', css: "", cssname: "", tpname: "cform",content:[{name: "姓名", value: ""}, {name: "年龄", value: ""},
  {name: "性别", value: ""},{name: "婚姻", value: ""},{name: "户口", value: ""},{name: "居住地", value: ""},{name: "工作经验(年)", value: ""}
  ,{name: "手机", value: ""},{name: "email", value: ""},{name: "其他联系方式", value: ""}]};

 jxt.job_intention.dataJson = { id: "", type: 'job_intention', css: "", cssname: "", tpname: "formlist",content:[{name: "工作性质", value: ""}, {name: "期望职位", value: ""},
  {name: "期望职业", value: ""},{name: "工作地区", value: ""}]};

 jxt.self_evaluation.dataJson={id: "", type: 'self_evaluation', css: "", cssname: "", tpname: "textdiv", content: '自我评价'};
 jxt.work_experience.dataJson={id: "", type: 'work_experience', css: "", cssname: "", tpname: "", content: [{starttime:'',endtime:'',company:'',position:'',job_description:''}]};
 jxt.project_experience.dataJson={id: "", type: 'project_experience', css: "", cssname: "", tpname: "", content: [{starttime:'',endtime:'',project_name:'',software_environment:"",development_tool:"",responsibility:""}]};
 jxt.imglist.dataJson={id: "", type: 'imglist', css: "", cssname: "", tpname: "", content:[]};

 /*
 *  jxti
 *  jxtdata
 *  tags  + 目录 +js
 *  templates + 目录
 *  jxtMsg
 *  page
 *
 * */