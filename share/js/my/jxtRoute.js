jxt.route={};
jxt.route.map={};
jxt.route.map["selfinfo"]="form";
jxt.route.map["job_intention"]="form";
jxt.route.map["self_evaluation"]="textdiv";

jxt.route.getFunc=function(funName,w) {
    if(!w)w=window;
    var fn=jxt.route.map[funName];
    if(fn)return jxt.route.getFunc(fn,w);
    var arr = funName.split('.');
    if( arr[0]!='jxt' ){
        arr.splice(0,0,'jxt');
    }
    var tagname=arr[1];
    var tagname_=jxt.route.map[tagname];
    if(  tagname_ ) {
        arr[1]=tagname_;
        return getFunc(w,arr);
    }

    return getFunc(w,arr);
};

jxt.route.exe =function (command,win,a1,aN) {
    var args = Array.prototype.slice.call(arguments);
    var att=args.slice(2);
   return  jxt.route.getFunc(command,win).apply(this,att);
};
