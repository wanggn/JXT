<?php



$path = $_SERVER['DOCUMENT_ROOT'] . '/templates';
$templates =$GLOBALS["templates"];// getfiles($path, 'dir');
foreach ($templates as $tag) {
    $_path = $path . '/' . $tag;
    $files = getfiles($_path, 'file');
    foreach ($files as $file) {
        $fullfilename = $_path . '/' . $file;
        if (str_replace(['/', '\\'], '', __FILE__) == str_replace(['/', '\\'], '', $fullfilename)) return;
        switch ($file) {
            case "readonly.html":
                echo '<script id="jhm_' . $tag . '" type="text/html"> <div data-jxt-type="{{type}}"  id="{{id}}" class="row {{css}}">   <div class="col-md-12">  <div class="tagdiv"  >   <div class="btn-clipboard">';
                if (file_exists($_path . '/readonlybar.html')) include_once($_path . '/readonlybar.html');

                echo ' <span data-jxt-eve="del" data-jxt-id="{{id}}">del</span><span data-jxt-eve="editbegin" data-jxt-id="{{id}}">edit</span></div>';
                include_once($fullfilename);
                echo '</div> </div>   </div></script>';
                break;
            case "edit.html":
                echo '<script id="jhm_' . $tag . '_edit" type="text/html">   <div data-jxt-type="{{type}}" id="{{id}}" class="row {{css}}">  <div class="col-md-12">     <div class="tagdiv-edit">   <div class="btn-clipboard btn-clipboard-edit">';
                if (file_exists($_path . '/editbar.html')) include_once($_path . '/editbar.html');
                echo '<span> 　 </span><span data-jxt-eve="del" data-jxt-id="{{id}}">del</span><span data-jxt-eve="ok" data-jxt-id="{{id}}">ok</span> </div>  ';
                include_once($fullfilename);
                echo '  </div>        </div>        </div>     </script>';
                break;
            case "poweredit.html":
                echo '<script id="jhm_' . $tag . '_poweredit" type="text/html">   <div data-jxt-type="{{type}}" id="{{id}}" class="row {{css}}">  <div class="col-md-12">     <div class="tagdiv-edit">   <div class="btn-clipboard btn-clipboard-edit">';
                if (file_exists($_path . '/editbar.html')) include_once($_path . '/editbar.html');
                echo '<span> 　 </span><span data-jxt-eve="del" data-jxt-id="{{id}}">del</span><span data-jxt-eve="ok" data-jxt-id="{{id}}">ok</span> </div>  ';
                include_once($fullfilename);
                echo '  </div>        </div>        </div>     </script>';
                break;
            case "bar.html":
            case "editbar.html":
            case "readonlybar.html":
                break;
            default:
                include_once($fullfilename);
                break;
        }
    }

}


?>

<style>
    .tagdiv{
        border: 1px solid #fff;
        padding: 15px;
        position: relative;

    }
    .tagdiv:hover{
        border: 1px solid #e3e3e3;
    }
    .tagdiv:hover .btn-clipboard{
        display: block;
    }
    .tagdiv-edit{
        border: 1px solid #002a80;
        padding: 15px;
        position: relative;

        border-radius: 5px;
    }
    .btn-clipboard {
        display:none;
        position: absolute;
        top: -8px;
        right: 0;
        z-index: 10;
        font-size: 12px;
        color: #767676;
        cursor: pointer;
    }
    .btn-clipboard-edit {
        position: absolute;
        top: -8px;
        right: 0;
        z-index: 10;
        display: block;
        font-size: 12px;
        color: #767676;
        cursor: pointer;
    }
    .btn-clipboard span {
        background-color: #fff;
        border: 1px solid #e1e1e8;
        border-radius: 4px  ;
        padding: 3px 5px;
        margin-right: 5px;
    }
    .btn-clipboard-edit span{
        border: 1px solid #002a80;
    }

</style>
